﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public static class DatabaseInitializer
    {
        public async static void Initialize(DataContext dbContext)
        {
            if (!dbContext.Roles.Any())
            {
                dbContext.Roles.AddRange(FakeDataFactory.Roles);
                dbContext.Employees.AddRange(FakeDataFactory.Employees);
                dbContext.Preferences.AddRange(FakeDataFactory.Preferences);
                dbContext.Customers.AddRange(FakeDataFactory.Customers);
                dbContext.CustomerPreferences.AddRange(FakeDataFactory.CustomerPreferences);
                dbContext.PromoCodes.AddRange(FakeDataFactory.PromoCodes);
            }
            await dbContext.SaveChangesAsync();
        }
    }
}
